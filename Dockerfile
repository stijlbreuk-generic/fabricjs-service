# https://github.com/geekduck/docker-node-canvas/blob/master/Dockerfile
FROM node:14-alpine3.11

# RUN apt-get update && apt-get install -qq build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev inkscape imagemagick
# RUN apt-get update && apt-get install -qq build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev

RUN apk add --no-cache \
      python \
      g++ \
      build-base \
      cairo-dev \
      jpeg-dev \
      pango-dev \
      musl-dev \
      giflib-dev \
      pixman-dev \
      pangomm-dev \
      libjpeg-turbo-dev \
      librsvg-dev \      
      freetype-dev

ENV NODE_OPTIONS=--max_old_space_size=4096

# Create app directory
WORKDIR /app
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
# Install app dependencies
RUN npm install

COPY tsconfig*.json ./
COPY src ./src
COPY fonts ./fonts

RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "start:prod" ]
