# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.8](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.7...v1.1.8) (2021-12-02)


### Bug Fixes

* docker issue ([b7a3dd4](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/b7a3dd469e4354d7f705f9d0554f620ae63996a5))
* memory leak ([17525de](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/17525de0b53a14c731362fac9c31a3bf9317bc12))

### [1.1.7](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.6...v1.1.7) (2021-08-31)


### Bug Fixes

* CI script ([7d802d1](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/7d802d16c046b9ec42e0d98121021643324b1f79))

### [1.1.6](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.5...v1.1.6) (2021-08-31)


### Bug Fixes

* font name spaces issue ([69ee973](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/69ee97361a5934b5a318171c3ad0fc9e93fa758a))

### [1.1.5](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.4...v1.1.5) (2021-03-31)


### Features

* including fonts ([ea9c7da](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/ea9c7da332cd591304373a9af9f48d5e9aacb2e0))

### [1.1.4](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.3...v1.1.4) (2021-03-29)

### [1.1.3](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.2...v1.1.3) (2021-03-29)


### Bug Fixes

* gitlab-ci ([f11c810](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/f11c81004d5c959f41651637230814b0c3a6d772))

### [1.1.2](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.1...v1.1.2) (2021-03-29)


### Bug Fixes

* gitlab ci ([0ebc4e2](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/0ebc4e257bf79d53b4c5225f82c404eb2ce7ab8e))

### [1.1.1](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.1.0...v1.1.1) (2021-03-29)


### Bug Fixes

* gitlabb ci ([fb41dc7](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/fb41dc71411230037051ccac6db973c775983107))

## [1.1.0](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.0.2...v1.1.0) (2021-03-29)


### Features

* alpine image ([20cc6db](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/20cc6db201a4e867929c55081debfb7c267ee68a))
* lint ([c66e90f](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/c66e90f282293e7e32dddfc0187e73247426ef2f))
* max memroy size ([05ac90c](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/05ac90c4fdaceaeccf9ccdb067d16317fd69041b))
* standard-sentry-setup ([f3b4183](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/f3b4183ad0e4dbed9abe61f7319d45f9ea59090d))


### Bug Fixes

* ci/cd ([60ae306](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/60ae306c1b0fad3810d7c7e8ab65101bf33ed200))
* dockerfile ([4619ccc](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/4619ccc7b9ee50ac1f8834757ec7c49192c1c887))
* github raw file refactor ([813a9ec](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/813a9ec65502ff7e9405f7e6d51d6eee7e886e58))
* npm audit fix ([71873a3](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/71873a37df3b691a119dc46460c7fcac99e1a635))

### [1.0.2](https://gitlab.com/stijlbreuk-generic/fabricjs-service/compare/v1.0.0...v1.0.2) (2020-11-11)


### Features

* env-cli ([f7a92c5](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/f7a92c57ac32231480ea351ecbe15ebc314ed994))
* standard-version ([920b038](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/920b038d692180054d02d48159e87ed0ca15ef85))


### Bug Fixes

* font names when contains space ([1f66194](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/1f66194464770ae5a638a3e991160ac2ea9dc86c))
* fonts directory ([36519ec](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/36519ec199fa816279650fcacfd42e42dd678ecb))
* lint+test ([173ed38](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/173ed383c51c0aadbf832cd88d828cb2dfa3c6e6))
* prometheus app ([4c5b336](https://gitlab.com/stijlbreuk-generic/fabricjs-service/commit/4c5b33662b6f73f78543500b66d5aed52d01f07e))
