import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getInfo(): { name: string; version: string } {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const { name, version } = require(`${process.cwd()}/package.json`);
    return { name, version };
  }
}
