import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { initSentry } from 'standard-sentry-setup';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  initSentry(app);
  const PORT = process.env.PORT || 3000;
  await app.listen(PORT);
  console.log(`Listening on port: ${PORT}`);
}
bootstrap();
