import { HttpService, Injectable } from '@nestjs/common';
import { registerFont } from 'canvas';
import { existsSync, writeFileSync } from 'fs';
import * as path from 'path';
import * as Sentry from '@sentry/node';

export interface FontFace {
  family: string;
  weight?: string;
  style?: '' | 'normal' | 'italic' | 'oblique';
}

const GOOGLE_FONTS_DIRECTORY = `${process.cwd()}/fonts/google/`;

@Injectable()
export class FontService {
  constructor(private readonly httpService: HttpService) {}

  private async downloadGoogleFont(fontFace: FontFace): Promise<string> {
    let style;
    switch (fontFace.style) {
      case 'italic':
        style = 'Italic';
      default:
        style = 'Regular';
    }
    const fontName = fontFace.family.replace(/ /g, '');
    const fontFileName = `${fontName}-${style}.ttf`;
    const fontPath = path.join(GOOGLE_FONTS_DIRECTORY, fontFileName);
    if (existsSync(fontPath)) {
      return fontPath;
    } else {
      // https://stackoverflow.com/a/60275751/2179157
      const url = `https://github.com/google/fonts/raw/main/ofl/${fontName.toLowerCase()}/${fontFileName}`;
      const { data } = await this.httpService.axiosRef.get(url, {
        responseType: 'arraybuffer',
      });
      writeFileSync(fontPath, data);
      return fontPath;
    }
  }

  public async installGoogleFont(fontFace: FontFace): Promise<void> {
    this.install(await this.downloadGoogleFont(fontFace), fontFace);
  }

  public install(path: string, fontFace: FontFace) {
    try {
      registerFont(path, fontFace);
      Sentry.captureMessage(
        `font "${fontFace.family}:${fontFace.style}:${fontFace.weight}" installed`,
        Sentry.Severity.Info,
      );
    } catch (error) {
      console.error(error);
      Sentry.captureMessage(
        `Installing "${fontFace.family}:${fontFace.style}:${fontFace.weight}" failed`,
        Sentry.Severity.Error,
      );
    }
  }
}
