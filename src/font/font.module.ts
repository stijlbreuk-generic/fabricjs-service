import { HttpModule, Module } from '@nestjs/common';
import { FontService } from './font.service';

@Module({
  imports: [HttpModule],
  providers: [FontService],
  exports: [FontService],
})
export class FontModule {}
