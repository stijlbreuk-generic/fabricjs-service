import { Module } from '@nestjs/common';
import { ExportService } from './export.service';
import { ExportController } from './export.controller';
import { FontModule } from '../font/font.module';

@Module({
  imports: [FontModule],
  providers: [ExportService],
  controllers: [ExportController],
})
export class ExportModule {}
