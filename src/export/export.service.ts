import * as jsdomUtils from 'jsdom/lib/jsdom/living/generated/utils';
import { Injectable } from '@nestjs/common';
import { createCanvas } from 'canvas';
import { fabric } from 'fabric';
import { WriteStream } from 'fs';
import { Measure } from '../utils/measure';
import { FontService } from '../font/font.service';

export interface FabricJSON {
  version: string;
  size: Size2D;
  objects: fabric.Object[];
}

export interface Size2D {
  width: number;
  height: number;
}

@Injectable()
export class ExportService {
  constructor(private readonly fontService: FontService) {}

  private async regitserGoogleFronts(objects: fabric.Text[]) {
    return Promise.all(
      objects.map((object) =>
        this.fontService.installGoogleFont({
          family: object.fontFamily,
          style: object.fontStyle,
          weight: object.fontWeight as string,
        }),
      ),
    );
  }

  private async loadFromJSON(
    fabricJSON: FabricJSON,
    element: HTMLCanvasElement,
    measure: Measure,
  ): Promise<fabric.StaticCanvas> {
    measure.startSubTask('loadFromJSON');
    // Registering Fonts
    const textObjects = fabricJSON.objects.filter(
      (object) => object.type == 'text',
    );
    await this.regitserGoogleFronts(textObjects as fabric.Text[]);
    // loading canvas from json
    const canvas = new fabric.StaticCanvas(element, fabricJSON.size as any);
    await new Promise((resolve) => canvas.loadFromJSON(fabricJSON, resolve));
    measure.doneSubTask('loadFromJSON');
    return canvas;
  }

  public async toSVG(
    fabricJSON: FabricJSON,
    options: fabric.IToSVGOptions = {},
  ): Promise<string> {
    const measure = new Measure('Canvas to SVG');
    measure.start();
    const canvas = await this.loadFromJSON(fabricJSON, null, measure);
    const svg = canvas.toSVG(options);
    canvas.dispose();
    measure.done();
    return svg;
  }

  public async toDataURL(
    fabricJSON: FabricJSON,
    options: fabric.IDataURLOptions = {},
  ): Promise<string> {
    const measure = new Measure('Canvas to DataURL');
    measure.start();
    const canvas = await this.loadFromJSON(fabricJSON, null, measure);
    const dataURL = canvas.toDataURL(options);
    canvas.dispose();
    measure.done();
    return dataURL;
  }

  private logMemory(prefix: string | number) {
    console.log(
      `${prefix}. Rss[${(+process.memoryUsage().rss / 1024 / 1024).toFixed(
        2,
      )}MB],Heap[${(+process.memoryUsage().heapUsed / 1024 / 1024).toFixed(
        2,
      )}MB]`,
    );
  }

  public async toPDF(fabricJSON: FabricJSON): Promise<WriteStream> {
    this.logMemory('starting');
    const measure = new Measure('Canvas to PDF');
    measure.start();
    const element = fabric['document'].createElement('canvas');
    const elementUnknowsProp = element[jsdomUtils.implSymbol];
    elementUnknowsProp._canvas = createCanvas(
      fabricJSON.size.width,
      fabricJSON.size.height,
      'pdf',
    ); // see "PDF Support" section
    const canvas = await this.loadFromJSON(fabricJSON, element, measure);
    const stream: WriteStream = fabric.util['getNodeCanvas'](
      canvas['lowerCanvasEl'],
    ).createPDFStream();
    return stream.on('end', () => {
      canvas.dispose();
      this.logMemory('before GC');
      global.gc();
      this.logMemory('after GC');
      measure.done();
    });
  }

  public async toPNG(fabricJSON: FabricJSON): Promise<WriteStream> {
    this.logMemory('starting');
    const measure = new Measure('Canvas to PNG');
    measure.start();
    const canvas = await this.loadFromJSON(fabricJSON, null, measure);
    const stream = canvas['createPNGStream']();
    return stream.on('end', () => {
      canvas.dispose();
      this.logMemory('before GC');
      global.gc();
      this.logMemory('after GC');
      measure.done();
    });
  }
}
