import { Body, Controller, Header, Post, Res, Response } from '@nestjs/common';
import { fabric } from 'fabric';
import { ExportService, FabricJSON } from './export.service';

@Controller('export')
export class ExportController {
  constructor(private readonly exportService: ExportService) {}

  @Post('/svg')
  @Header('content-type', 'image/svg+xml')
  async toSVG(
    @Body('fabricJson') fabricJson: FabricJSON,
    @Body('options') options: fabric.IToSVGOptions = {},
  ) {
    return this.exportService.toSVG(fabricJson, options);
  }

  @Post('/data-url')
  @Header('content-type', 'image/svg+xml')
  async toDataURL(
    @Body('fabricJson') fabricJson: FabricJSON,
    @Body('options') options: fabric.IDataURLOptions = {},
  ) {
    return this.exportService.toDataURL(fabricJson, options);
  }

  @Post('/pdf')
  @Header('content-type', 'application/pdf')
  async toPDF(@Body() fabricJson: FabricJSON, @Res() res: Response) {
    console.log(fabricJson);
    const stream = await this.exportService.toPDF(fabricJson);
    stream.pipe(res as any);
  }

  @Post('/png')
  @Header('content-type', 'image/png')
  async toPNG(@Body() fabricJson: FabricJSON, @Res() res: Response) {
    const stream = await this.exportService.toPNG(fabricJson);
    stream.pipe(res as any);
  }
}
