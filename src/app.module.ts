import { Module } from '@nestjs/common';
import { RavenModule, RavenInterceptor } from 'nest-raven';
import { PromModule } from '@digikare/nestjs-prom';
import { APP_INTERCEPTOR } from '@nestjs/core';
//
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ExportModule } from './export/export.module';
import { FontModule } from './font/font.module';

@Module({
  imports: [
    PromModule.forRoot({
      defaultLabels: {
        app: 'fabricjs-service',
      },
    }),
    RavenModule,
    ExportModule,
    FontModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useValue: new RavenInterceptor(),
    },
  ],
})
export class AppModule {}
