import * as shortid from 'shortid';

export class Measure {
  private readonly id: string;
  constructor(title: string, id = shortid()) {
    this.id = `${title} #${id}`;
  }
  public start(): void {
    console.log(`${this.id} started`);
    console.time(this.id);
  }
  public startSubTask(title: string): void {
    console.log(`${this.id} > ${title} started`);
    console.time(`${this.id} > ${title}`);
  }
  public doneSubTask(title: string): void {
    console.timeEnd(`${this.id} > ${title}`);
  }
  public done(): void {
    console.timeEnd(this.id);
  }
}
